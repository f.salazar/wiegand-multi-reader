#include <Wiegand.h>

// WIEGAND PINS for use
byte WIEGAND1_D0 = 36;
byte WIEGAND1_D1 = 39;
byte WIEGAND2_D0 = 34;
byte WIEGAND2_D1 = 35;

WIEGAND wg;

// EXAMPLE FOR TWO WIEGAND

void handler1D1()
{
	wg.defineD0(1);
}

void handler1D0()
{
	wg.defineD1(1);
}

void handler2D1()
{
	wg.defineD0(2);
}

void handler2D0()
{
	wg.defineD1(2);
}

void setup()
{
	Serial.begin(115200); // 115200 case esp32 or your baudrate needed for example arduino 9600
	wg.begin(WIEGAND1_D0, WIEGAND1_D1, 1, handler1D0, handler1D1);
	wg.begin(WIEGAND2_D0, WIEGAND2_D1, 2, handler2D0, handler2D1);
}

void loop()
{
	if (wg.available(1) || wg.available(2))
	{
		String wiegand = String(wg.getWiegandActive());
		String code = String(wg.getCode(wiegand.toInt()));
		String protocol = String(wg.getWiegandType(wiegand.toInt()));

		if (code.length() < 4)
			return; // Filter for not read noise

		Serial.print("Wiegand HEX = ");
		Serial.print(code, HEX);
		Serial.print(", DECIMAL = ");
		Serial.print(code);
		Serial.print(", Type W");
		Serial.print(protocol);
		Serial.print(", Wiegand #");
		Serial.println(wiegand);
	}
}