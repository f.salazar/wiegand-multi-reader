# Remaster version based on original library

| Library | Description |
| ---| --- |
| [Wiegand library](https://github.com/monkeyboard/Wiegand-Protocol-Library-for-Arduino) | This is the easiest and cleanest library to use when only a single wiegand reader is needed. I strongly suggest to use this version if you only need one reader support |


# Wiegand 4 bit, 8 bit, 24 bit, 26 bit, 32 bit and 34 bit library for Arduino

The Wiegand interface is a de facto standard commonly used to connect a card reader or keypad to an electronic entry system. Wiegand interface has the ability to transmit signal over long distance with a simple 3 wires connection. This library uses interrupt pins from Arduino to read the pulses from Wiegand interface and return the code and type of the Wiegand.

## Requirements

The following are needed 

* [Arduino](http://www.arduino.cc) or [ESP32](https://www.espressif.com/en/products/socs/esp32)
* [Wiegand RFID Reader] with frecuency 125Mhz NOT 13Khz
* DATA0 or D0 and DATA1 or D1 of Wiegand connects to Arduino/ESP32 (case ESP32 any pin has compatible) pin compatible with attachInterrupt

## Installation 

Create a folder named Wiegand in Arduino's libraries folder.  You will have the following folder structure:

	cd arduino/libraries
	git clone https://gitlab.com/f.salazar/wiegand-multi-reader.git Wiegand


## Contributors

[monkeyboard](https://github.com/monkeyboard) first version of library

[Jose Granados (Joseg99)](https://gitlab.com/Joseg99) contribuitor to maintenance of new features of library

*This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation; either version 2.1 of the License, or (at your option) any later version.*

*This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.*
