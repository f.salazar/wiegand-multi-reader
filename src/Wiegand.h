#ifndef _WIEGAND_H
#define _WIEGAND_H

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

enum WiegandFrecuency
{
	WIEGAND_125_KHZ,
	WIEGAND_13_MHZ
};

class WIEGAND
{
	typedef struct WiegandConfig
	{
		volatile unsigned long cardTempHigh = 0;
		volatile unsigned long cardTemp = 0;
		volatile unsigned long lastWiegand = 0;
		volatile int bitCount = 0;
		int wiegandType = 0;
		unsigned long code = 0;
		int activeDoor;
		byte frecuency = 0;
		String codeTemp = "";
	} WiegandConfig_t;

private:
	static byte _activeWiegand;
	static WiegandConfig_t _wiegandConfig[3];
	static bool _available(byte wiegand);
	static void _readD0(byte wiegand);
	static void _readD1(byte wiegand);
	static void _resetTempVariables(byte wiegand);
	static bool _wiegandHasBits125(byte wiegand, unsigned long sysTick);
	static bool _wiegandHasBits13(byte wiegand);
	static bool _wiegandParseBits125(byte wiegand, unsigned long sysTick);
	static bool _wiegandParseBits13(byte wiegand);
	static bool _doWiegandConversion(byte wiegand);
	static void _loadInterrupts(byte d0, byte d1, void (*handlerD0)(), void (*handlerD1)());
	static unsigned long _getCardId(volatile unsigned long *codehigh, volatile unsigned long *codelow, char bitlength);
	static char _translateEnterEscapeKeyPress(char originalKeyPress);

public:
	WIEGAND();
	void begin(int pinD0, int pinD1, byte wiegand, void (*handlerD0)(), void (*handlerD1)(), byte frecuency = WIEGAND_125_KHZ);
	void defineD0(byte wiegand);
	void defineD1(byte wiegand);
	void updateFrecuency(byte wiegand, byte frecuency);
	int getWiegandActive();
	bool available(byte wiegand);
	unsigned long getCode(byte wiegand);
	int getWiegandType(byte wiegand);
	String getWiegandFrecuency(byte wiegand);
};

#endif
