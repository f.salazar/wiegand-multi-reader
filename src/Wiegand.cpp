#include "Wiegand.h"

#if defined(ESP8266)
#define INTERRUPT_ATTR ICACHE_RAM_ATTR
#elif defined(ESP32)
#define INTERRUPT_ATTR IRAM_ATTR
#else
#define INTERRUPT_ATTR
#endif

WIEGAND::WiegandConfig_t WIEGAND::_wiegandConfig[3];
byte WIEGAND::_activeWiegand = 0;

WIEGAND::WIEGAND()
{
}

bool WIEGAND::_available(byte wiegand)
{
	bool ret = _doWiegandConversion(wiegand);
	return ret;
}

void WIEGAND::_loadInterrupts(byte d0, byte d1, void (*handlerD0)(), void (*handlerD1)())
{
	attachInterrupt(digitalPinToInterrupt(d0), handlerD0, FALLING); // Hardware interrupt - high to low pulse
	attachInterrupt(digitalPinToInterrupt(d1), handlerD1, FALLING); // Hardware interrupt - high to low pulse
}

void WIEGAND::_readD0(byte wiegand)
{
	byte frecuency = _wiegandConfig[wiegand].frecuency;
	_wiegandConfig[wiegand].lastWiegand = millis(); // Keep track of last wiegand bit received
	_wiegandConfig[wiegand].bitCount++;							// Increment bit count for Interrupt connected to D0

	switch (frecuency)
	{
	case WIEGAND_125_KHZ:

		if (_wiegandConfig[wiegand].bitCount > 31) // If bit count more than 31, process high bits
		{
			_wiegandConfig[wiegand].cardTempHigh |= ((0x80000000 & _wiegandConfig[wiegand].cardTemp) >> 31); //	shift value to high bits
			_wiegandConfig[wiegand].cardTempHigh <<= 1;
			_wiegandConfig[wiegand].cardTemp <<= 1;
		}
		else
		{
			_wiegandConfig[wiegand].cardTemp <<= 1; // D0 represent binary 0, so just left shift card data
		}
		break;
	case WIEGAND_13_MHZ:
		_wiegandConfig[wiegand].codeTemp += "0";
		break;
	default:
		Serial.println("INVALID WIEGAND FRECUENCY, ONLY USE 'WIEGAND_125_KHZ' OR 'WIEGAND_13_MHZ'");
		break;
	}
}

void WIEGAND::_readD1(byte wiegand)
{
	byte frecuency = _wiegandConfig[wiegand].frecuency;
	_wiegandConfig[wiegand].lastWiegand = millis(); // Keep track of last wiegand bit received
	_wiegandConfig[wiegand].bitCount++;							// Increment bit count for Interrupt connected to D1

	switch (frecuency)
	{
	case WIEGAND_125_KHZ:

		if (_wiegandConfig[wiegand].bitCount > 31) // If bit count more than 31, process high bits
		{
			_wiegandConfig[wiegand].cardTempHigh |= ((0x80000000 & _wiegandConfig[wiegand].cardTemp) >> 31); // shift value to high bits
			_wiegandConfig[wiegand].cardTempHigh <<= 1;
			_wiegandConfig[wiegand].cardTemp |= 1;
			_wiegandConfig[wiegand].cardTemp <<= 1;
		}
		else
		{
			_wiegandConfig[wiegand].cardTemp |= 1;	// D1 represent binary 1, so OR card data with 1 then
			_wiegandConfig[wiegand].cardTemp <<= 1; // left shift card data
		}
		break;
	case WIEGAND_13_MHZ:
		_wiegandConfig[wiegand].codeTemp += "1";
		break;
	default:
		Serial.println("INVALID WIEGAND FRECUENCY, ONLY USE 'WIEGAND_125_KHZ' OR 'WIEGAND_13_MHZ'");
		break;
	}
}

unsigned long WIEGAND::_getCardId(volatile unsigned long *codehigh, volatile unsigned long *codelow, char bitlength)
{
	if (bitlength == 26) // EM tag
		return (*codelow & 0x1FFFFFE) >> 1;

	if (bitlength == 24)
		return (*codelow & 0x7FFFFE) >> 1;

	if (bitlength == 34) // Mifare
	{
		*codehigh = *codehigh & 0x03; // only need the 2 LSB of the codehigh
		*codehigh <<= 30;							// shift 2 LSB to MSB
		*codelow >>= 1;
		return *codehigh | *codelow;
	}

	if (bitlength == 32)
	{
		return (*codelow & 0x7FFFFFFE) >> 1;
	}

	return *codelow; // EM tag or Mifare without parity bits
}

char WIEGAND::_translateEnterEscapeKeyPress(char originalKeyPress)
{
	switch (originalKeyPress)
	{
	case 0x0b:		 // 11 or * key
		return 0x0d; // 13 or ASCII ENTER

	case 0x0a:		 // 10 or # key
		return 0x1b; // 27 or ASCII ESCAPE

	default:
		return originalKeyPress;
	}
}

void WIEGAND::_resetTempVariables(byte wiegand)
{
	_wiegandConfig[wiegand].bitCount = 0;
	_wiegandConfig[wiegand].cardTemp = 0;
	_wiegandConfig[wiegand].cardTempHigh = 0;
	_wiegandConfig[wiegand].codeTemp = "";
}

bool WIEGAND::_wiegandParseBits125(byte wiegand, unsigned long sysTick)
{
	_wiegandConfig[wiegand].cardTemp >>= 1; // shift right 1 bit to get back the real value - interrupt done 1 left shift in advance
	int bits = _wiegandConfig[wiegand].bitCount;

	switch (bits)
	{
	case 4:
	{
		// 4-bit Wiegand codes have no data integrity check so we just
		// read the LOW nibble.
		_wiegandConfig[wiegand].code = (int)_translateEnterEscapeKeyPress(_wiegandConfig[wiegand].cardTemp & 0x0000000F);
		_wiegandConfig[wiegand].wiegandType = bits;
		_resetTempVariables(wiegand);
		return true;
		break;
	}
	case 8:
	{
		// 8-bit Wiegand keyboard data, high nibble is the "NOT" of low nibble
		// eg if key 1 pressed, data=E1 in binary 11100001 , high nibble=1110 , low nibble = 0001
		char highNibble = (_wiegandConfig[wiegand].cardTemp & 0xf0) >> 4;
		char lowNibble = (_wiegandConfig[wiegand].cardTemp & 0x0f);
		_wiegandConfig[wiegand].wiegandType = bits;
		_resetTempVariables(wiegand);

		if (lowNibble != (~highNibble & 0x0f)) // check if low nibble matches the "NOT" of high nibble.
		{
			_wiegandConfig[wiegand].lastWiegand = sysTick;
			_resetTempVariables(wiegand);
			return false;
		}
		_wiegandConfig[wiegand].code = (int)_translateEnterEscapeKeyPress(lowNibble);
		return true;
		break;
	}
	case 24:
	case 26:
	case 32:
	case 34:
	{
		if (bits > 32) // bit count more than 32 bits, shift high bits right to make adjustment
			_wiegandConfig[wiegand].cardTempHigh >>= 1;

		unsigned long cardTempHigh = _wiegandConfig[wiegand].cardTempHigh;
		unsigned long cardTemp = _wiegandConfig[wiegand].cardTemp;
		unsigned long cardID = _getCardId(&cardTempHigh, &cardTemp, bits);

		_wiegandConfig[wiegand].wiegandType = bits;
		_wiegandConfig[wiegand].code = cardID;
		_resetTempVariables(wiegand);
		return true;
		break;
	}
	default:
		_wiegandConfig[wiegand].lastWiegand = sysTick;
		_resetTempVariables(wiegand);
		return false;
		break;
	}
}

bool WIEGAND::_wiegandParseBits13(byte wiegand)
{
	int bits = _wiegandConfig[wiegand].bitCount;

	switch (bits)
	{
	case 26:
	case 34:
	{
		int finalBits = bits - 1;
		byte startBits = 9;
		String code = _wiegandConfig[wiegand].codeTemp;
		String cardCode = code.substring(startBits, finalBits);
		_wiegandConfig[wiegand].code = strtoull(cardCode.c_str(), NULL, 2);
		_wiegandConfig[wiegand].wiegandType = bits;
		_resetTempVariables(wiegand);
		return true;
		break;
	}
	default:
		Serial.println("INVALID BITS FOUND " + String(bits));
		_resetTempVariables(wiegand);
		return false;
		break;
	}
}

bool WIEGAND::_wiegandHasBits125(byte wiegand, unsigned long sysTick)
{
	// bitCount for keypress=4 or 8, Wiegand 26=24 or 26, Wiegand 34=32 or 34
	int bits = _wiegandConfig[wiegand].bitCount;
	switch (bits)
	{
#ifdef DECODE_4_BITS
	case 4:
#endif
#ifdef DECODE_8_BITS
	case 8:
#endif
	case 24:
	case 26:
	case 32:
	case 34:
		return true;
		break;
	default:
		// well time over 25 ms and bitCount !=8 , !=26, !=34 , must be noise or nothing then.
		_wiegandConfig[wiegand].lastWiegand = sysTick;
		_resetTempVariables(wiegand);
		return false;
	}
}

bool WIEGAND::_wiegandHasBits13(byte wiegand)
{
	int bits = _wiegandConfig[wiegand].bitCount;

	if (bits > 34 || bits < 26)
		return false;

	return true;
}

bool WIEGAND::_doWiegandConversion(byte wiegand)
{
	unsigned long sysTick = millis();

	byte frecuency = _wiegandConfig[wiegand].frecuency;

	if ((sysTick - _wiegandConfig[wiegand].lastWiegand) > 25) // if no more signal coming through after 25ms
	{
		switch (frecuency)
		{
		case WIEGAND_125_KHZ:
		{
			bool wiegandBits = _wiegandHasBits125(wiegand, sysTick);
			if (!wiegandBits)
				return wiegandBits;

			bool bitsParsed = _wiegandParseBits125(wiegand, sysTick);
			return bitsParsed;
		}
		break;
		case WIEGAND_13_MHZ:
		{
			bool wiegandBits = _wiegandHasBits13(wiegand);
			if (!wiegandBits)
			{
				_resetTempVariables(wiegand);
				return wiegandBits;
			}
			bool bitsParsed = _wiegandParseBits13(wiegand);
			return bitsParsed;
		}
		break;
		default:
			return false;
			break;
		}
	}
	else
		return false;
}

unsigned long WIEGAND::getCode(byte wiegand)
{
	return _wiegandConfig[wiegand].code;
}

int WIEGAND::getWiegandType(byte wiegand)
{
	return _wiegandConfig[wiegand].wiegandType;
}

String WIEGAND::getWiegandFrecuency(byte wiegand)
{
	return _wiegandConfig[wiegand].frecuency == WIEGAND_125_KHZ ? "125Khz" : "13.56Mhz";
}

int WIEGAND::getWiegandActive()
{
	return _activeWiegand;
}

bool WIEGAND::available(byte wiegand)
{
	bool available = _available(wiegand);
	if (available)
		_activeWiegand = wiegand;
	return available;
}

INTERRUPT_ATTR void WIEGAND::defineD0(byte wiegand)
{
	_readD0(wiegand);
}

INTERRUPT_ATTR void WIEGAND::defineD1(byte wiegand)
{
	_readD1(wiegand);
}

void WIEGAND::updateFrecuency(byte wiegand, byte frecuency)
{
	_wiegandConfig[wiegand].frecuency = frecuency;
}

void WIEGAND::begin(int pinD0, int pinD1, byte wiegand, void (*handlerD0)(), void (*handlerD1)(), byte frecuency)
{
	_wiegandConfig[wiegand].frecuency = frecuency;

	pinMode(pinD0, INPUT); // Set D0 pin as input
	pinMode(pinD1, INPUT); // Set D1 pin as input

	_loadInterrupts(pinD0, pinD1, handlerD0, handlerD1);
}
